#include "mainwindow.h"
#include "ui_mainwindow.h"


using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    string full_name = ui->textEdit->toPlainText().toStdString();
    string name, surname;
    int pos = 0;
    int spaces = 0;

    for (auto it : full_name)
    {
        if (it == ' ')
        {
            spaces++;
        }
    }
    if (spaces != 1)
    {
        cout << "bledny format" << endl;
        return;
    }
    for (auto it : full_name)
    {
        if (it == ' ')
        {
            name.assign(full_name.begin(), full_name.begin() + pos);
            break;
        }
        pos++;
    }
    surname.assign(full_name.begin() + pos + 1, full_name.end());
    names.push_back(make_pair (name,surname));
}
